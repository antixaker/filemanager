﻿using FileManager.Services;
using FileManager.Services.CppFileManager;
using FileManager.Services.ResultWriter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Services.FileManager;
using System.Threading.Tasks;

namespace FileManager.UnitTests
{
    [TestClass]
    public class CppFileManagerTests
    {
        private Mock<IFileScanner> _mockFileScanner;
        private Mock<IResultWriter> _mockResultWriter;
        private Fixture fixture = new Fixture();
        private string _rootDirectoryPathFixture;
        private string _resultFilePathFixture;

        [TestMethod]
        public async Task ManageFiles_ScannerInvokesWithCppFilter_True()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            const string cppSearchPattern = "*.cpp";

            _mockFileScanner = new Mock<IFileScanner>();
            _mockResultWriter = new Mock<IResultWriter>();
            IFileManager cppManager = new CppFileManager(_mockFileScanner.Object, _mockResultWriter.Object);

            // Act
            await cppManager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockFileScanner.Verify(x => x.GetFileNames(It.IsAny<string>(), cppSearchPattern));
        }

        [TestMethod]
        public async Task ManageFiles_ScannerReturnsValues_WroteToResultFileWithEndSlash()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            var resultSet = new[] { "C://sample.cpp", "D://progr.cpp" };
            var managedSet = new[] { "C://sample.cpp/", "D://progr.cpp/" };

            _mockFileScanner = new Mock<IFileScanner>();
            _mockFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(resultSet));
            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager cppManager = new CppFileManager(_mockFileScanner.Object, _mockResultWriter.Object);

            // Act
            await cppManager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(managedSet, It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public async Task ManageFiles_ScannerReturnsNull_WriterNotInvoke()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            _mockFileScanner = new Mock<IFileScanner>();
            _mockFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult<string[]>(null));
            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager cppManager = new CppFileManager(_mockFileScanner.Object, _mockResultWriter.Object);

            // Act
            await cppManager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(It.IsAny<string[]>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public async Task ManageFiles_InvokeWithoutResultFilePathArgument_WriteToDefaultFile()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();

            _mockFileScanner = new Mock<IFileScanner>();
            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager cppManager = new CppFileManager(_mockFileScanner.Object, _mockResultWriter.Object);

            // Act
            await cppManager.ManageFiles(_rootDirectoryPathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(It.IsAny<string[]>(), It.Is<string>(str => str.Equals(Constants.DefaultResultsFilePath))), Times.Once);
        }

    }
}
