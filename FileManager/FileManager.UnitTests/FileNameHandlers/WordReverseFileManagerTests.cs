﻿using FileManager.Services;
using FileManager.Services.ResultWriter;
using FileManager.Services.WordReverseFileManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Services.FileManager;
using System.Threading.Tasks;

namespace FileManager.UnitTests
{
    [TestClass]
    public class WordReverseFileManagerTests
    {
        private Mock<IFileScanner> _stubFileScanner;
        private Mock<IResultWriter> _mockResultWriter;

        private Fixture fixture = new Fixture();
        private string _rootDirectoryPathFixture;
        private string _resultFilePathFixture;

        [TestMethod]
        public async Task ManageFiles_DirectoryHasFiles_ReturnsReversedByWord()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            var stubFileSet = new[] { "dir\\subdir\\hello.txt", "dir\\sample.cpp" };
            var stubReversedFileSet = new[] { "hello.txt\\subdir\\dir", "sample.cpp\\dir" };
            _stubFileScanner = new Mock<IFileScanner>();
            _stubFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(stubFileSet));
            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager manager = new WordReverseFileManager(_stubFileScanner.Object, _mockResultWriter.Object);

            // Act
            await manager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(stubReversedFileSet, It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public async Task ManageFiles_ScannerReturnsNull_WriterNotInvoke()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();

            _stubFileScanner = new Mock<IFileScanner>();
            _stubFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult<string[]>(null));
            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager manager = new WordReverseFileManager(_stubFileScanner.Object, _mockResultWriter.Object);

            // Act
            await manager.ManageFiles(_rootDirectoryPathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(It.IsAny<string[]>(), It.IsAny<string>()), Times.Never);
        }

    }
}
