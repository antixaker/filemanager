﻿using FileManager.Services;
using FileManager.Services.ResultWriter;
using FileManager.Services.SimpleFileManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Services.FileManager;
using System.Threading.Tasks;

namespace FileManager.UnitTests
{
    [TestClass]
    public class SimpleFileManagerTests
    {
        private Mock<IFileScanner> _stubFileScanner;
        private Mock<IResultWriter> _mockResultWriter;

        private Fixture fixture = new Fixture();
        private string _rootDirectoryPathFixture;
        private string _resultFilePathFixture;

        [TestMethod]
        public async Task ManageFiles_ScannerReturnsValues_WroteToResultFile()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            var resultSet = new[] { "C://sample", "D://text.txt" };

            _stubFileScanner = new Mock<IFileScanner>();
            _stubFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(resultSet));

            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager fileManager = new SimpleFileManager(_stubFileScanner.Object, _mockResultWriter.Object);

            // Act
            await fileManager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(resultSet, It.IsAny<string>()), Times.Once);
        }


        [TestMethod]
        public async Task ManageFiles_ScannerReturnsNull_WriterNotInvoke()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();
            _resultFilePathFixture = fixture.Create<string>();

            _stubFileScanner = new Mock<IFileScanner>();
            _stubFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult<string[]>(null));

            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager fileManager = new SimpleFileManager(_stubFileScanner.Object, _mockResultWriter.Object);

            // Act
            await fileManager.ManageFiles(_rootDirectoryPathFixture, _resultFilePathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(It.IsAny<string[]>(), It.IsAny<string>()), Times.Never);
        }


        [TestMethod]
        public async Task ManageFiles_InvokeWithoutResultFilePath_WriteToDefaultFile()
        {
            // Arrange
            _rootDirectoryPathFixture = fixture.Create<string>();

            var resultSet = new[] { "C://sample", "D://text.txt" };

            _stubFileScanner = new Mock<IFileScanner>();
            _stubFileScanner.Setup(x => x.GetFileNames(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(resultSet));

            _mockResultWriter = new Mock<IResultWriter>();

            IFileManager fileManager = new SimpleFileManager(_stubFileScanner.Object, _mockResultWriter.Object);

            // Act
            await fileManager.ManageFiles(_rootDirectoryPathFixture);

            // Assert
            _mockResultWriter.Verify(x => x.WriteResults(It.IsAny<string[]>(), It.Is<string>(str => str.Equals(Constants.DefaultResultsFilePath))), Times.Once);
        }
    }
}
