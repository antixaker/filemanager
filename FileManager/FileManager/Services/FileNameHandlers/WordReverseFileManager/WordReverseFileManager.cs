﻿using FileManager.Services.ResultWriter;
using Services.FileManager;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FileManager.Services.WordReverseFileManager
{
    public class WordReverseFileManager : IFileManager
    {
        private const string Separator = "\\";
        private IFileScanner _fileScanner;
        private IResultWriter _resultWriter;

        public WordReverseFileManager(IFileScanner fileScanner, IResultWriter resultWriter)
        {
            _fileScanner = fileScanner;
            _resultWriter = resultWriter;
        }

        public Task ManageFiles(string rootDirectory)
        {
            return ManageFiles(rootDirectory, Constants.DefaultResultsFilePath);
        }

        public async Task ManageFiles(string rootDirectory, string resultFilePath)
        {
            var fileNames = await _fileScanner.GetFileNames(rootDirectory);

            if (fileNames == null)
            {
                return;
            }

            var wordReversedFileNames = fileNames
                .Select(x =>
                    string.Join<string>(Separator, x
                    .Split(new string[] { Separator }, StringSplitOptions.RemoveEmptyEntries)
                    .Reverse()))
                .ToArray();

            _resultWriter.WriteResults(wordReversedFileNames, resultFilePath);
        }
    }
}
