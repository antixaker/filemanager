﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Services
{
    public interface IFileManager
    {
        Task ManageFiles(string rootDirectory, string resultFilePath);
        Task ManageFiles(string rootDirectory);
    }
}
