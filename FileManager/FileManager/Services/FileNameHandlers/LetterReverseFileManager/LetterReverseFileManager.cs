﻿using FileManager.Services.ResultWriter;
using Services.FileManager;
using System.Linq;
using System.Threading.Tasks;

namespace FileManager.Services.LetterReverseFileManager
{
    public class LetterReverseFileManager : IFileManager
    {
        private IFileScanner _fileScanner;
        private IResultWriter _resultWriter;

        public LetterReverseFileManager(IFileScanner fileScanner, IResultWriter resultWriter)
        {
            _fileScanner = fileScanner;
            _resultWriter = resultWriter;
        }

        public Task ManageFiles(string rootDirectory)
        {
            return ManageFiles(rootDirectory, Constants.DefaultResultsFilePath);
        }

        public async Task ManageFiles(string rootDirectory, string resultFilePath)
        {
            var fileNames = await _fileScanner.GetFileNames(rootDirectory);

            if (fileNames == null)
            {
                return;
            }

            var letterReversedFileNames = fileNames.Select(x => new string(x.Reverse().ToArray())).ToArray();

            _resultWriter.WriteResults(letterReversedFileNames, resultFilePath);
        }
    }
}
