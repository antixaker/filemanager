﻿using FileManager.Services.ResultWriter;
using Services.FileManager;
using System.Threading.Tasks;

namespace FileManager.Services.SimpleFileManager
{
    public class SimpleFileManager : IFileManager
    {
        private IFileScanner _fileScanner;
        private IResultWriter _resultWriter;

        public SimpleFileManager(IFileScanner fileScanner, IResultWriter resultWriter)
        {
            _fileScanner = fileScanner;
            _resultWriter = resultWriter;
        }

        public Task ManageFiles(string rootDirectory)
        {
            return ManageFiles(rootDirectory, Constants.DefaultResultsFilePath);
        }

        public async Task ManageFiles(string rootDirectory, string resultFilePath)
        {
            var fileNames = await _fileScanner.GetFileNames(rootDirectory);

            if (fileNames == null)
            {
                return;
            }

            _resultWriter.WriteResults(fileNames, resultFilePath);
        }
    }
}
