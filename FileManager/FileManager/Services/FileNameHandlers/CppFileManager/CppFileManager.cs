﻿using FileManager.Services.ResultWriter;
using Services.FileManager;
using System.Linq;
using System.Threading.Tasks;

namespace FileManager.Services.CppFileManager
{
    public class CppFileManager : IFileManager
    {
        private const string CppFileFilter = "*.cpp";
        private const string ForwardSlash = "/";
        private IFileScanner _fileScanner;
        private IResultWriter _resultWriter;

        public CppFileManager(IFileScanner fileScanner, IResultWriter resultWriter)
        {
            _fileScanner = fileScanner;
            _resultWriter = resultWriter;
        }

        public Task ManageFiles(string rootDirectory)
        {
            return ManageFiles(rootDirectory, Constants.DefaultResultsFilePath);
        }

        public async Task ManageFiles(string rootDirectory, string resultFilePath)
        {
            var fileNames = await _fileScanner.GetFileNames(rootDirectory, CppFileFilter);

            if (fileNames == null)
            {
                return;
            }

            var cppFileNames = fileNames.Select(x=>x+ForwardSlash).ToArray();

            _resultWriter.WriteResults(cppFileNames, resultFilePath);
        }
    }
}
