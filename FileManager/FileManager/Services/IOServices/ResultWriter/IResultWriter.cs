﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Services.ResultWriter
{
    public interface IResultWriter
    {
        void WriteResults(string[] results, string resultFilePath);
    }
}
