﻿using System.IO;
using System.Linq;

namespace FileManager.Services.ResultWriter
{
    public class ResultWriter : IResultWriter
    {
        public void WriteResults(string[] results, string resultFilePath)
        {
            if (!results.Any())
            {
                return;
            }

            File.WriteAllLines(resultFilePath, results);
        }
    }
}
