﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.FileManager
{
    public interface IFileScanner
    {
        Task<string[]> GetFileNames(string rootDirectory, string searchPattern = "*.*");
    }
}
