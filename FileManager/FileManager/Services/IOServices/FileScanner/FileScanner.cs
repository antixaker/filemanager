﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Services.FileManager
{
    public class FileScanner : IFileScanner
    {
        public Task<string[]> GetFileNames(string rootDirectory, string searchPattern="*.*")
        {
            if (!Directory.Exists(rootDirectory))
            {
                return Task.FromResult<string[]>(null);
            }

            return Task.Run(()=> Directory.GetFiles(rootDirectory, searchPattern, SearchOption.AllDirectories).Select(x=>x.TrimStart((rootDirectory+"\\").ToArray())).ToArray());
        }
    }
}
