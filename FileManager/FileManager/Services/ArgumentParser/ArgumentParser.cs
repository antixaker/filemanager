﻿using Microsoft.Practices.Unity;
using System;
using System.Threading.Tasks;

namespace FileManager.Services.ArgumentParser
{
    public class ArgumentParser
    {
        public static Task HandleArguments(IUnityContainer resolver, string[] arguments)
        {
            if (arguments == null || arguments.Length < 2)
            {
                return Task.FromResult(0);
            }

            var rootDirectory = arguments[0];
            var handlerKey = arguments[1];

            IFileManager handlerType = GetHandlerTypeByKey(resolver, handlerKey);

            if (arguments.Length >= 3)
            {
                var resultsFilePath = arguments[2];
                return handlerType.ManageFiles(rootDirectory, resultsFilePath);
            }
            else
            {
                return handlerType.ManageFiles(rootDirectory);
            }
        }

        private static IFileManager GetHandlerTypeByKey(IUnityContainer resolver, string handlerKey)
        {
            switch (handlerKey)
            {
                case Constants.FileActionAll:
                    return resolver.Resolve<SimpleFileManager.SimpleFileManager>(handlerKey);
                case Constants.FileActionCpp:
                    return resolver.Resolve<CppFileManager.CppFileManager>(handlerKey);
                case Constants.FileActionLetterReverse:
                    return resolver.Resolve<LetterReverseFileManager.LetterReverseFileManager>(handlerKey);
                case Constants.FileActionWordReverse:
                    return resolver.Resolve<WordReverseFileManager.WordReverseFileManager>(handlerKey);
                default:
                    throw new ArgumentException($"Unknown argument '{handlerKey}' was received");
            }
        }
    }
}