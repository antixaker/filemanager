﻿using FileManager.Services.ArgumentParser;
using FileManager.Services.CppFileManager;
using FileManager.Services.LetterReverseFileManager;
using FileManager.Services.ResultWriter;
using FileManager.Services.SimpleFileManager;
using FileManager.Services.WordReverseFileManager;
using Microsoft.Practices.Unity;
using Services.FileManager;
using System;
using System.IO;

namespace FileManager
{
    class Program
    {
        static IUnityContainer Resolver { get; } = new UnityContainer();

        static Program()
        {
            RegisterServices();
        }

        static void Main(string[] args)
        {
            try
            {
                ArgumentParser.HandleArguments(Resolver, args).Wait();
            }
            catch (Exception ex)
            {
                File.WriteAllText("error.txt", ex.Message);
                throw;
            }
        }

        private static void RegisterServices()
        {
            var injectionCtorParams = new InjectionConstructor(new FileScanner(), new ResultWriter());
            Resolver.RegisterType<SimpleFileManager>(Constants.FileActionAll,injectionCtorParams);
            Resolver.RegisterType<CppFileManager>(Constants.FileActionCpp, injectionCtorParams);
            Resolver.RegisterType<WordReverseFileManager>(Constants.FileActionWordReverse, injectionCtorParams);
            Resolver.RegisterType<LetterReverseFileManager>(Constants.FileActionLetterReverse, injectionCtorParams);
        }
    }
}
