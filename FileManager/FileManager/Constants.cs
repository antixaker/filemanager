﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public class Constants
    {
        public const string DefaultResultsFilePath = "results.txt";

        public const string FileActionAll = "all";

        public const string FileActionCpp = "cpp";

        public const string FileActionWordReverse = "reversed1";

        public const string FileActionLetterReverse = "reversed2";
    }
}
