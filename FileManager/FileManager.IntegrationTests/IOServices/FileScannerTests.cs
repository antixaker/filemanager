﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.FileManager;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FileManager.IntegrationTests
{
    [TestClass]
    public class FileScannerTests
    {
        private const string WorkDirectory = "C://dir";

        [TestMethod]
        public async Task GetFileNames_DirectoryNotExists_ReturnNull()
        {
            // Arrange
            var service = new FileScanner();

            // Act
            var results = await service.GetFileNames(WorkDirectory);

            // Assert
            Assert.IsNull(results);
        }


        [TestMethod]
        public async Task GetFileNames_EmptyDirectoryExists_ReturnsEmptyArray()
        {
            // Arrange
            var service = new FileScanner();
            Directory.CreateDirectory(WorkDirectory);

            // Act
            var results = await service.GetFileNames(WorkDirectory);

            // Assert
            Assert.IsTrue(results.Length == 0);
        }


        [TestMethod]
        public async Task GetFileNames_DirectoryContainsFile_ReturnsFileNameWithRelativePath()
        {
            // Arrange
            var service = new FileScanner();
            var fileName = "file.txt";
            CreateFileInWorkDirectory(fileName);

            // Act
            var results = await service.GetFileNames(WorkDirectory);

            // Assert
            Assert.AreEqual(1, results?.Length);
            Assert.AreEqual(fileName, results[0]);
        }

        [TestMethod]
        public async Task GetFileNames_DirectoryWithFilesAndSubdirectory_ReturnsOnlyFilesName()
        {
            // Arrange
            var service = new FileScanner();
            var fileName = "file.txt";
            CreateFileInWorkDirectory(fileName);

            var subdirectoryPath = "C://dir\\subdir";
            Directory.CreateDirectory(subdirectoryPath);

            // Act
            var results = await service.GetFileNames(WorkDirectory);

            // Assert
            Assert.AreEqual(1, results?.Length);
            Assert.AreEqual(fileName, results[0]);
        }

        [TestMethod]
        public async Task GetFileNames_DirectoryWithFileAndSubdirectoryWithFile_ReturnsAllRealtiveFilePathes()
        {
            // Arrange
            var service = new FileScanner();
            var fileName = "file.txt";
            CreateFileInWorkDirectory(fileName);

            var subdir = "subdir";
            var subdirectoryPath = WorkDirectory + "\\" + subdir;
            Directory.CreateDirectory(subdirectoryPath);
            var subFileName = "subfile.txt";
            var relativePath = subdir + "\\" + subFileName;
            var fullFileNameInSubdirectory = WorkDirectory + "\\" + relativePath;
            using (File.Create(fullFileNameInSubdirectory)) ;

            // Act
            var results = await service.GetFileNames(WorkDirectory);

            // Assert
            var resultList = new List<string>(results);

            Assert.IsTrue(resultList.Capacity == 2);
            Assert.IsTrue(resultList.Contains(fileName));
            Assert.IsTrue(resultList.Contains(relativePath));
        }


        [TestMethod]
        public async Task GetFileNames_SetSearchFilterForTxtExtention_ReturnsOnlyTxtFileRelativePathes()
        {
            // Arrange
            var service = new FileScanner();
            var fullFileNameWithPath = "C://dir\\file.doc";
            Directory.CreateDirectory(WorkDirectory);
            using (File.Create(fullFileNameWithPath)) ;

            var subdirectoryPath = "C://dir\\subdir";
            Directory.CreateDirectory(subdirectoryPath);
            var relativePath = "subdir\\subfile.txt";
            var fullFileNameInSubdirectory = "C://dir" + "\\" + relativePath;
            using (File.Create(fullFileNameInSubdirectory)) ;

            // Act
            var results = await service.GetFileNames(WorkDirectory, "*.txt");

            // Assert
            var resultList = new List<string>(results);

            Assert.IsTrue(resultList.Capacity == 1);
            Assert.IsTrue(resultList.Contains(relativePath));
        }

        [TestCleanup]
        public void ClearWorkaround()
        {
            if (Directory.Exists(WorkDirectory))
            {
                Directory.Delete(WorkDirectory, true);
            }
        }

        private void CreateFileInWorkDirectory(string fileName)
        {
            var fullFileNameWithPath = WorkDirectory + "\\" + fileName;
            Directory.CreateDirectory(WorkDirectory);
            using (File.Create(fullFileNameWithPath)) ;
        }
    }
}
