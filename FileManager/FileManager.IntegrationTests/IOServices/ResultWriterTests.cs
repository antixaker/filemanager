﻿using FileManager.Services.ResultWriter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace FileManager.IntegrationTests
{
    [TestClass]
    public class ResultWriterTests
    {
        private const string ResultFilePath = "D://result.txt";
        private const string ResultFileNameWithoutPath = "results.txt";
        private const string ResultValueSample = "C://sampleValue";

        [TestMethod]
        public void WriteResults_PassEmptyResults_DontCreateFileWithResults()
        {
            // Arrange
            var service = new ResultWriter();

            // Act
            service.WriteResults(new string[] { }, ResultFilePath);

            // Assert
            Assert.IsFalse(File.Exists(ResultFilePath));
        }

        [TestMethod]
        public void WriteResults_PassOneResultValue_ResultFileCreated()
        {
            // Arrange
            var service = new ResultWriter();

            // Act
            service.WriteResults(new [] { ResultValueSample }, ResultFilePath);

            // Assert
            Assert.IsTrue(File.Exists(ResultFilePath));
        }

        [TestMethod]
        public void WriteResults_PassOneResultValue_SavedToResultFile()
        {
            // Arrange
            var service = new ResultWriter();

            // Act
            service.WriteResults(new [] { ResultValueSample }, ResultFilePath);
            var textFromFile = File.ReadLines(ResultFilePath);
            
            // Assert
            Assert.IsTrue(textFromFile.Count() == 1);
            Assert.AreEqual(ResultValueSample, textFromFile.ToList()[0]);
        }


        [TestMethod]
        public void WriteResults_PassResultFileNameWithoutPath_CreateFileInExeDirectory()
        {
            // Arrange
            var service = new ResultWriter();

            // Act
            service.WriteResults(new[] { ResultValueSample }, ResultFileNameWithoutPath);

            // Assert
            Assert.IsTrue(File.Exists(ResultFileNameWithoutPath));
        }

        [TestCleanup]
        public void ClearWorkaround()
        {
            if (File.Exists(ResultFilePath))
            {
                File.Delete(ResultFilePath);
            }
            if (File.Exists(ResultFileNameWithoutPath))
            {
                File.Delete(ResultFileNameWithoutPath);
            }
        }
    }
}
